# README #

Autodesk Maya Plug-in for export and import skin weights.

### dcSkin.py ###

* dcSkin.py: MAYA Python API plug-in for Autodesk Maya 2014 / 15.
* Platorms: Windows.
* Version: 1.0
* More information: [Wiki](https://bitbucket.org/DavidCuellar/dcskin/wiki/Home)

### How do I get set up? ###

* Setting up: Copy the dcSkin.py into your Autodesk Maya plugins directory. Load it on the Plug-in Manager Window
* Configuration: None
* Dependencies: None

### How to run it ###

#### Open UI ####
* MEL COMMAND: dcSkin
* PYTHON COMMAND: maya.cmds.dcSkin() or: import maya.cmds as cmds; cmds.dcSkin()
#### Export Skin ####
* MEL COMMAND: dcSkin -sm "name of the skin geometry" -p "full path skin data file"
* PYTHON COMMAND: maya.cmds.dcSkin( sm="name of the skin geometry", p="full path skin data file")
#### Import Skin ####
* MEL COMMAND: dcSkin -tm "target mesh" -p "full path skin data file"
* PYTHON COMMAND: maya.cmds.dcSkin( tm="target mesh", p="full path skin data file")

### Contact ###

* Created by [David Cuéllar](http://davidcuellar.es)