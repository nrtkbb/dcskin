# python

# --------------------------------------------------------------------------------
#  dcSkin.py
# Author: David Cuellar
# Website: http://www.davidcuellar.es
# Email: david.cuellar@gmail.com
# Wiki: https://bitbucket.org/DavidCuellar/dcskin/wiki/Home
# Date: 20 July 2015
# Version: 1.0
#
# Release Notes:
# 1.0- First release
#
# Autodesk Maya Plug-in to import / export skin weights
# Copyright (C) 2015  David Cuellar
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# --------------------------------------------------------------------------------

import os
import cPickle
import datetime
import maya.cmds as cmds
import maya.OpenMaya as OpenMaya
import maya.OpenMayaMPx as OpenMayaMPx
import maya.OpenMayaAnim as OpenMayaAnim

pluginName = 'dcSkin'

# --------------------------------------------------------------------------------
# arguments flags
# --------------------------------------------------------------------------------

helpFlag = '-h'
helpFlagLong = '-help'

sourceMeshFlag = '-sm'
sourceMeshFlagLong = '-exportMesh'

targetMeshFlag = '-tm'
targetMeshFlagLong = '-importMesh'

pathFlag = '-p'
pathFlagLong = '-path'

# --------------------------------------------------------------------------------
# help information
# --------------------------------------------------------------------------------

helpText = ''
helpText += '\n Description: Import/Export the skin weights of/on a mesh.'
helpText += '\n'
helpText += '\n Flags: dcSkin            -h        -help            <n/a>        help message'
helpText += '\n                          -sm       -sourceMesh      <string>     the name of the source mesh'
helpText += '\n                          -tm       -targetMesh      <string>     the name of the target mesh'
helpText += '\n                          -p        -path            <string>     full path to the skin data file'
helpText += '\n Usage: Execute the command with the following arguments:'
helpText += '\n To Export: dcSkin -em <source mesh> -p <skin data file> '
helpText += '\n To Import: dcSkin -im <target mesh> -p <skin data file> '


# --------------------------------------------------------------------------------
# command
# --------------------------------------------------------------------------------


class dcSkin(OpenMayaMPx.MPxCommand):
    def __init__(self):
        OpenMayaMPx.MPxCommand.__init__(self)
        self.windowName = 'dcSkinWindow'
        self.sourceMesh = None
        self.targetMesh = None
        self.dataFile = None
        self.skinCluster = None
        self.skinData = {}
        self.operation = None

    def doIt(self, argList):
        self.dagModifier = OpenMaya.MDagModifier()
        # --------------------------------------------------------------------------------
        # parse the arguments
        # --------------------------------------------------------------------------------
        argData = OpenMaya.MArgDatabase(self.syntax(), argList)
        # help flag
        if argData.isFlagSet(helpFlag):
            self.setResult(helpText)
            return
        # source mesh flag
        if argData.isFlagSet(sourceMeshFlag):
            self.sourceMesh = argData.flagArgumentString(sourceMeshFlag, 0)
        # target mesh flag
        if argData.isFlagSet(targetMeshFlag):
            self.targetMesh = argData.flagArgumentString(targetMeshFlag, 0)
        # path flag
        if argData.isFlagSet(pathFlag):
            self.dataFile = argData.flagArgumentString(pathFlag, 0)
        # Run it
        self.run()
        return self.redoIt()

    def run(self):
        """Check how to run the command. UI or command mode."""
        if self.sourceMesh == None and self.targetMesh == None:
            self.ui()
        else:
            if self.verify_arguments():
                if self.operation == 'export':
                    self.export_skin()
                elif self.operation == 'import':
                    self.import_skin()

    def verify_arguments(self):
        """ Function that verifies the parameters passed in the command flags """
        # Check if the export flag is active
        if self.sourceMesh is not None:
            # Check if the object exist in the scene
            if cmds.objExists(self.sourceMesh):
                # Check if it is a mesh
                if cmds.objectType(cmds.listRelatives(self.sourceMesh)[0]) == 'mesh':
                    # Check if it has a skin cluster in the history
                    history = cmds.listHistory(self.sourceMesh, pdo=True, il=2)
                    if history is not None:
                        for node in history:
                            if cmds.nodeType(node) == 'skinCluster':
                                self.skinCluster = node
                                self.operation = 'export'
                                # Check if the data path exist
                                if os.path.exists(os.path.split(str(self.dataFile))[0]):
                                    return True
                                else:
                                    OpenMaya.MGlobal.displayError('{0} it is not a valid path.'.format(self.dataFile))
                                    return False
                            else:
                                OpenMaya.MGlobal.displayError('{0} does not have skin cluster.'.format(self.sourceMesh))
                                return False
                else:
                    OpenMaya.MGlobal.displayError('The object: {0} is not a mesh.'.format(self.sourceMesh))
                    return False
            else:
                OpenMaya.MGlobal.displayError('The object: {0} does not exist on the scene.'.format(self.sourceMesh))
                return False
        # Check if the import flag is active
        if self.targetMesh is not None:
            # Check if the object exist in the scene
            if cmds.objExists(self.targetMesh):
                # Check if the object is a mesh
                if cmds.objectType(cmds.listRelatives(self.targetMesh)[0]) == 'mesh':
                    # Check if the data file exists
                    if os.path.isfile(self.dataFile):
                        # read the data file
                        with open(self.dataFile, 'r') as dataFile:
                            self.skinData = cPickle.load(dataFile)
                        self.operation = 'import'
                        return True
                    else:
                        OpenMaya.MGlobal.displayError('Data file: {0} not found.'.format(self.dataFile))
                        return False
                else:
                    OpenMaya.MGlobal.displayError('The object: {0} is not a mesh.'.format(self.targetMesh))
                    return False
            else:
                OpenMaya.MGlobal.displayError('The object: {0} does not exist on the scene.'.format(self.targetMesh))
                return False

    # --------------------------------------------------------------------------------
    # UI functions
    # --------------------------------------------------------------------------------

    def ui(self):
        """  Build the UI window """
        self.close()
        cmds.window(self.windowName, title='dcSkin Export/Import plug-in', sizeable=False, retain=True)
        cmds.menuBarLayout()
        cmds.menu(label='Help', helpMenu=True)
        cmds.menuItem(label='Wiki site', c=self.wiki)
        cmds.menuItem(label='About', c=self.about)
        cmds.columnLayout(columnAttach=('both', 5), columnWidth=550)
        cmds.frameLayout('scf1', borderVisible=True, labelVisible=True, h=85, li=10, label='dcSkin', marginWidth=5,
                         marginHeight=5)
        cmds.formLayout(numberOfDivisions=25)
        cmds.columnLayout(columnAttach=('both', 5), columnWidth=550)
        cmds.rowLayout(numberOfColumns=2)
        cmds.textFieldGrp('mesh', label='Source/Target Mesh', text='Type a mesh name')
        cmds.button(label='Get from selection', width=100, c=self.get_selected_mesh)
        cmds.setParent('..')
        cmds.rowLayout(numberOfColumns=2)
        cmds.textFieldGrp('path', label='Skin Data File', text='...skin data file path...')
        cmds.button(label='...', width=100, c=self.get_data_path)
        cmds.setParent('..')
        cmds.setParent('..')
        cmds.setParent('..')
        cmds.setParent('..')
        cmds.frameLayout('scf2', borderVisible=True, labelVisible=False, h=40, li=10, label='Operations', marginWidth=5,
                         marginHeight=5)
        cmds.formLayout(numberOfDivisions=100)
        cmds.rowLayout(numberOfColumns=2)
        cmds.button(label='Export Skin Data', width=260, c=self.save_skin_weights)
        cmds.button(label='Import Skin Data', width=260, c=self.load_skin_weights)
        cmds.setParent('..')
        cmds.showWindow()

    def close(self):
        """ close any instance of the plugin window"""
        if cmds.window(self.windowName, q=True, exists=True):
            cmds.deleteUI(self.windowName)

    def wiki(*args):
        """ Open the wiki page"""
        cmds.showHelp('https://bitbucket.org/DavidCuellar/dcskin/wiki/Home', absolute=True)

    def about(*args):
        """ Print the plug-in information"""
        print ('-') * 50
        print 'ABOUT THIS PLUGIN'
        for param in ['name', 'path', 'vendor', 'version', 'apiVersion', 'command']:
            info = eval('cmds.pluginInfo("dcSkin.py", query=True, {0}=True)'.format(param))
            print '{0}: {1}'.format(param.capitalize(), info)
        print 'Copyright (C) 2015  David Cuellar \n' \
              'This program comes with ABSOLUTELY NO WARRANTY; \n' \
              'This is free software, and you are welcome to redistribute it  \n' \
              'under certain conditions; see  <http://www.gnu.org/licenses/>'

    def get_selected_mesh(*args):
        """UI function to get the first selected mesh"""
        sel = cmds.ls(sl=True)
        for obj in sel:
            if cmds.objectType(cmds.listRelatives(obj)[0]) == 'mesh':
                cmds.textFieldGrp('mesh', e=True, tx=obj)
                return True
            else:
                OpenMaya.MGlobal.displayError('The object: {0} is not a mesh'.format(obj))

    def get_data_path(self, *args):
        """ UI function to get the path to store the weights """
        try:
            self.dataFile = cmds.fileDialog2(caption='Save Skin Data File', fm=0, ff='Skin Data (*.dc_skn)')
            cmds.textFieldGrp('path', e=True, tx=self.dataFile[0])
        except:
            OpenMaya.MGlobal.displayError('Cancel by user.')
            return False
        else:
            return self.dataFile

    def save_skin_weights(self, *args):
        """Export the skin weights through the ui"""
        # Initialize the variables with the UI values
        self.targetMesh = None
        self.sourceMesh = cmds.textFieldGrp('mesh', query=True, text=True)
        self.dataFile = cmds.textFieldGrp('path', query=True, text=True)
        if os.path.exists(os.path.split(str(self.dataFile))[0]):
            pass
        else:
            try:
                self.dataFile = cmds.fileDialog2(caption='Save Skin Data File', fm=0, ff='Skin Data (*.dc_skn)')[0]
            except:
                OpenMaya.MGlobal.displayError('Cancel by user.')
                return False
        # Verify the arguments
        if self.verify_arguments():
            self.export_skin()
        return self.skinData

    def load_skin_weights(self, *args):
        """Import the skin weights through the ui """
        # Initialize the variables with the UI values
        self.sourceMesh = None
        self.targetMesh = cmds.textFieldGrp('mesh', query=True, text=True)
        self.dataFile = cmds.textFieldGrp('path', query=True, text=True)
        if os.path.isfile(str(self.dataFile)):
            pass
        else:
            try:
                self.dataFile = \
                    cmds.fileDialog2(fileMode=1, caption='Select Skin Data File', fm=1, ff='Skin Data (*.dc_skn)')[0]
            except:
                OpenMaya.MGlobal.displayError('Cancel by user.')
                return False
        if self.verify_arguments():
            if self.import_skin():
                return True
            else:
                OpenMaya.MGlobal.displayError('An error has occur during the load of the skin weights.')

    # --------------------------------------------------------------------------------
    # Common functions
    # --------------------------------------------------------------------------------

    @staticmethod
    def get_mobject(objName):
        """
        get the MObject of an object
        :param objName: string of the object
        :return: MObject
        """
        sel = OpenMaya.MSelectionList()
        sel.add(str(objName))
        obj = OpenMaya.MObject()
        sel.getDependNode(0, obj)
        return obj

    @staticmethod
    def get_mdag_path(objName):
        """
        get dagPath of an obj
        :param objName: string
        :return: MDagPath Object
        """
        sel = OpenMaya.MSelectionList()
        sel.add(str(objName))
        obj = OpenMaya.MDagPath()
        sel.getDagPath(0, obj)
        return obj

    def get_components(self, objName):
        """
        get all the component of the mesh
        :param objName: string name of the mesh
        :return: MObject
        """
        selectionList = OpenMaya.MSelectionList()
        allComponents = OpenMaya.MObject()
        selComponents = cmds.polyListComponentConversion(objName, ff=1, fe=1, fuv=1, fvf=1, fv=1, tv=1)
        sel = cmds.ls(selComponents)
        cmds.select(sel, r=1)
        OpenMaya.MGlobal.getActiveSelectionList(selectionList)
        selectionList.getDagPath(0, self.get_mdag_path(objName), allComponents)
        cmds.select(cl=True)
        return allComponents

    # --------------------------------------------------------------------------------
    # Export skin functions
    # --------------------------------------------------------------------------------

    def export_skin(self):
        """
        Function that export the skin weights in a file
        :return: path to the file, skinData
        """
        startTime = datetime.datetime.now()
        # Get the SkinCluster Node Object
        skinNode = OpenMayaAnim.MFnSkinCluster(self.get_mobject(self.skinCluster))
        # Get the mesh DAG path and the mesh components
        meshPath = self.get_mdag_path(self.sourceMesh)
        # Get the components of the mesh
        components = self.get_components(self.sourceMesh)
        # Create a variable to store the weights
        weights = OpenMaya.MDoubleArray()
        # Create a variable to store the blendWeights
        blendWeights = OpenMaya.MDoubleArray()
        # create a list to store the influence objects.
        self.skinData['Influence Objects'] = []
        # create a list to store the influence objects ids.
        self.skinData['Influence Ids Array'] = []
        # create a list to store a list with empty weights.
        self.skinData['Zero Weights Array'] = []
        # create a list to store the mesh weights.
        self.skinData['Weights Array'] = []
        # get the MDagPath for all influences of the SkinCluster
        infDags = OpenMaya.MDagPathArray()
        skinNode.influenceObjects(infDags)
        for i in range(infDags.length()):
            infPath = infDags[i].partialPathName()
            infId = int(skinNode.indexForInfluenceObject(infDags[i]))
            self.skinData['Influence Objects'].append(infPath)
            self.skinData['Influence Ids Array'].append(infId)
            self.skinData['Zero Weights Array'].append(0)
        # creating pointer object for to be used in MFnSkinCluster's get weight function
        util = OpenMaya.MScriptUtil()
        countPointer = util.asUintPtr()
        OpenMaya.MScriptUtil.setUint(countPointer, 0)
        # get the weights
        skinNode.getWeights(meshPath, components, weights, countPointer)
        # Store the weights into the skin data dictionary
        self.skinData['Weights Array'] = list(weights)
        # get the blendWeights
        skinNode.getBlendWeights(meshPath, components, blendWeights)
        # Store the blendWeights into the skin data dictionary
        self.skinData['BlendWeights Array'] = list(blendWeights)
        # Storing the skin data
        with open(self.dataFile, 'w') as dataFile:
            cPickle.dump(self.skinData, dataFile)
        print ('-') * 50
        print 'Skin Weights saved successfully. \n' \
              'Geometry: {0} \n' \
              'File: {1} \n' \
              'Time: {2}'.format(self.sourceMesh, self.dataFile, datetime.datetime.now() - startTime)
        return self.dataFile, self.skinData

    # --------------------------------------------------------------------------------
    # Import skin functions
    # --------------------------------------------------------------------------------

    def import_skin(self):
        """
        Import the skin weight to the the target mesh
        :return: bool
        """
        startTime = datetime.datetime.now()
        self.prepare_target_mesh()
        if self.apply_weights():
            print 'Skin Weights imported successfully. \n' \
                  'Geometry: {0} \n' \
                  'File: {1} \n' \
                  'Time: {2}'.format(self.targetMesh, self.dataFile, datetime.datetime.now() - startTime)
            return True
        else:
            return False

    def prepare_target_mesh(self):
        """ Check if the target mesh has skin cluster, if it has one, detach it and add one to match the influences id's
         with the skinData loaded, else, add one."""
        addSkin = True
        history = cmds.listHistory(self.targetMesh, pdo=True, il=2)
        if history is not None:
            for node in history:
                # If the mesh has skin cluster detach it so we can apply the influence objects in the same id order
                if cmds.nodeType(node) == 'skinCluster':
                    cmds.skinCluster(self.targetMesh, e=True, ub=True)
                    break
        if addSkin:
            self.skinCluster = self.add_skin_cluster(self.skinData['Influence Objects'], self.targetMesh)

    @staticmethod
    def add_skin_cluster(influenceObjs, mesh):
        """Add the skin cluster to the target mesh
        :param: dictionary with the skin influences
        :param: string with the name of the mesh
        :return: string skin cluster name"""
        influences = ''
        # Apply the same id order for the influences
        for i in range(len(influenceObjs)):
            influences = influences + '"' + influenceObjs[i] + '", '
        exp = 'cmds.skinCluster({0} "{1}", tsb=True)'.format(influences, mesh)
        return eval(exp)[0]

    def apply_weights(self):
        """
        Apply the skin weight to the mesh
        :return: bool
        """
        print ('-') * 50
        # SkinCluster Node
        skinNode = OpenMayaAnim.MFnSkinCluster(self.get_mobject(self.skinCluster))
        # Mesh DAG Path
        mshDAGpath = self.get_mdag_path(self.targetMesh)
        # Mesh components
        components = self.get_components(self.targetMesh)
        # Influences Id's array
        inflsId = OpenMaya.MIntArray()
        # zero weights
        zeroWeights = OpenMaya.MFloatArray()
        # Weights double array
        weights = OpenMaya.MFloatArray()
        # Converting the python list to the proper type
        util = OpenMaya.MScriptUtil()
        util.createIntArrayFromList(self.skinData['Influence Ids Array'], inflsId)
        util.createFloatArrayFromList(self.skinData['Zero Weights Array'], zeroWeights)
        util.createFloatArrayFromList(self.skinData['Weights Array'], weights)
        util2 = OpenMaya.MScriptUtil()
        util2.createFromList(self.skinData['BlendWeights Array'], len(self.skinData['BlendWeights Array']))
        blendWeights = OpenMaya.MDoubleArray(util2.asDoublePtr(), len(self.skinData['BlendWeights Array']))
        # Zero the default weights to be sure the values are clean
        skinNode.setWeights(mshDAGpath, components, inflsId, zeroWeights, True)
        # Set the stored weights
        skinNode.setWeights(mshDAGpath, components, inflsId, weights, True)
        # Set the stored blendWeights
        skinNode.setBlendWeights(mshDAGpath, components, blendWeights)
        return True

    # --------------------------------------------------------------------------------

    def redoIt(self):
        self.dagModifier.doIt()

    def undoIt(self):
        self.dagModifier.undoIt()

    def isUndoable(self):
        return True


def creator():
    return OpenMayaMPx.asMPxPtr(dcSkin())


def syntaxCreator():
    syn = OpenMaya.MSyntax()
    syn.addFlag(helpFlag, helpFlagLong)
    syn.addFlag(sourceMeshFlag, sourceMeshFlagLong, OpenMaya.MSyntax.kString)
    syn.addFlag(targetMeshFlag, targetMeshFlagLong, OpenMaya.MSyntax.kString)
    syn.addFlag(pathFlag, pathFlagLong, OpenMaya.MSyntax.kString)
    return syn


def initializePlugin(obj):
    plugin = OpenMayaMPx.MFnPlugin(obj, 'David Cuellar', '1.0', 'Any')
    try:
        plugin.registerCommand(pluginName, creator, syntaxCreator)
    except:
        raise RuntimeError, 'Failed to register command'


def uninitializePlugin(obj):
    plugin = OpenMayaMPx.MFnPlugin(obj)
    try:
        plugin.deregisterCommand(pluginName)
    except:
        raise RuntimeError, ('Failed to unregister command: %s\n' % pluginName)
